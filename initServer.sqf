// initServer.sqf
// Executes only on the server at mission start
// No parameters are passed to this script

// Call the template initServer function
[] call XPT_fnc_initServer; // DO NOT CHANGE THIS LINE

// Call the script to handle initial task setup
[] execVM "scripts\tasks.sqf";



//////////////////////////////////////////////////////////
///// Add any mission specific code after this point /////
//////////////////////////////////////////////////////////

// Create a list of mission objects that should not be curator editable
XPT_blacklistedMissionObjects = [];

// Define some mission variables
sxp_hostage_bagango_rescued = 0;
sxp_hostage_corazol_rescued = 0;
sxp_hostage_everon_rescued = 0;
sxp_hostage_mercalillo_rescued = 0;
sxp_hostage_obregan_rescued = 0;