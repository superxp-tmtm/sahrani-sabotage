// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// UH-1 Transport
	case (toLower "CUP_I_UH1H_Slick_RACS"):
	{
		[_newVeh, "heli"] call XPT_fnc_loadItemCargo;
	};
	
	// UH-1 Gunship
	case (toLower "CUP_I_UH1H_gunship_RACS"):
	{
		[
			_newVeh,
			["RACS_Navy",1], 
			true
		] call BIS_fnc_initVehicle;
		
		_newVeh setPylonLoadout ["pylons1", "CUP_PylonPod_CMFlare_Chaff", false, [-1]];
		_newVeh setPylonLoadout ["pylons2", "CUP_PylonPod_CMFlare_Chaff", false, [-1]];
		_newVeh setPylonLoadout ["pylons3", "CUP_PylonPod_7Rnd_Rocket_FFAR_M", false, [-1]];
		_newVeh setPylonLoadout ["pylons4", "CUP_PylonPod_7Rnd_Rocket_FFAR_M", false, [-1]];
		_newVeh setPylonLoadout ["pylons5", "CUP_PylonPod_2000Rnd_TE5_Red_Tracer_762x51_M134A_M", false, [-1]];
		_newVeh setPylonLoadout ["pylons6", "CUP_PylonPod_2000Rnd_TE5_Red_Tracer_762x51_M134A_M", false, [-1]];
		
		[_newVeh, "heli"] call XPT_fnc_loadItemCargo;
	};
	
	// LAV-25 M240
	case (toLower "CUP_I_LAV25M240_RACS"):
	{
		[_newVeh, "lav"] call XPT_fnc_loadItemCargo;
	};
	
	// Land Rover Transport
	case (toLower "CUP_I_LR_Transport_RACS"):
	{
		[_newVeh, "vehicle"] call XPT_fnc_loadItemCargo;
	};
	
	// MTVR Transport
	case (toLower "CUP_I_MTVR_RACS"):
	{
		[_newVeh, "vehicle"] call XPT_fnc_loadItemCargo;
	};
	
	// MTVR Logistics
	case (toLower "CUP_I_MTVR_Refuel_RACS");
	case (toLower "CUP_I_MTVR_Repair_RACS");
	case (toLower "CUP_I_MTVR_Ammo_RACS"):
	{
		[_newVeh, "vehicle"] call XPT_fnc_loadItemCargo;
	};
};