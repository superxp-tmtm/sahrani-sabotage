// Function for updating mission tasks when objectives are completed.
if (!isServer) exitWith {};
switch (toLower (_this select 0)) do {

	// CORAZOL
	case "corazol_vip": {
		// Check how many rescued hostages we have compared to the total count
		// Assume that this will only be run if all hostages are rescued, or if over half of them have died
		private _hostageCount = count ((getMissionLayerEntities "Corazol VIPs") select 0);
		if (sxp_hostage_corazol_rescued >= (_hostageCount / 2)) then {
			["corazol_vip","SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			["corazol_vip","FAILED"] call BIS_fnc_taskSetState;
		};
		["corazol_parent"] spawn SXP_fnc_updateTask;
	};
	case "corazol_parent": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["corazol_vip"]) == 0) then {
			["corazol_parent","SUCCEEDED",false] call BIS_fnc_taskSetState;
			["mission_complete"] spawn SXP_fnc_updateTask;
		};
	};
	
	// BAGANGO
	case "bagango_hostages": {
		private _hostageCount = count ((getMissionLayerEntities "Bagango Hostages") select 0);
		if (sxp_hostage_bagango_rescued >= (_hostageCount / 2)) then {
			["bagango_hostages","SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			["bagango_hostages","FAILED"] call BIS_fnc_taskSetState;
		};
		["bagango_parent"] spawn SXP_fnc_updateTask;
	};
	case "bagango_tv_station": {
		["bagango_tv_station","SUCCEEDED"] call BIS_fnc_taskSetState;
		["bagango_parent"] spawn SXP_fnc_updateTask;
	};
	case "bagango_parent": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["bagango_hostages","bagango_tv_station"]) == 0) then {
			["bagango_parent","SUCCEEDED",false] call BIS_fnc_taskSetState;
			["mission_complete"] spawn SXP_fnc_updateTask;
		};
	};
	
	// EVERON
	case "everon_hostages": {
		private _hostageCount = count ((getMissionLayerEntities "Everon Hostages") select 0);
		if (sxp_hostage_everon_rescued >= (_hostageCount / 2)) then {
			["everon_hostages","SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			["everon_hostages","FAILED"] call BIS_fnc_taskSetState;
		};
		["everon_parent"] spawn SXP_fnc_updateTask;
	};
	case "everon_military": {
		["everon_military","SUCCEEDED"] call BIS_fnc_taskSetState;
		["everon_parent"] spawn SXP_fnc_updateTask;
	};
	case "everon_parent": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["everon_hostages","everon_military"]) == 0) then {
			["everon_parent","SUCCEEDED",false] call BIS_fnc_taskSetState;
			["mission_complete"] spawn SXP_fnc_updateTask;
		};
	};
	
	// MERCALILLO
	case "mercalillo_hostages": {
		private _hostageCount = count ((getMissionLayerEntities "Mercalillo Hostages") select 0);
		if (sxp_hostage_mercalillo_rescued >= (_hostageCount / 2)) then {
			["mercalillo_hostages","SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			["mercalillo_hostages","FAILED"] call BIS_fnc_taskSetState;
		};
		["mercalillo_parent"] spawn SXP_fnc_updateTask;
	};
	case "mercalillo_parent": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["mercalillo_hostages"]) == 0) then {
			["mercalillo_parent","SUCCEEDED",false] call BIS_fnc_taskSetState;
			["mission_complete"] spawn SXP_fnc_updateTask;
		};
	};
	
	// OBREGAN
	case "obregan_hostages": {
		private _hostageCount = count ((getMissionLayerEntities "Obregan Hostages") select 0);
		if (sxp_hostage_obregan_rescued >= (_hostageCount / 2)) then {
			["obregan_hostages","SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			["obregan_hostages","FAILED"] call BIS_fnc_taskSetState;
		};
		["obregan_parent"] spawn SXP_fnc_updateTask;
	};
	case "obregan_military": {
		["obregan_military","SUCCEEDED"] call BIS_fnc_taskSetState;
		["obregan_parent"] spawn SXP_fnc_updateTask;
	};
	case "obregan_parent": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["obregan_hostages","obregan_military"]) == 0) then {
			["obregan_parent","SUCCEEDED",false] call BIS_fnc_taskSetState;
			["mission_complete"] spawn SXP_fnc_updateTask;
		};
	};
	
	// MISSION COMPLETE
	case "mission_complete": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["bagango_parent","corazol_parent","everon_parent","mercalillo_parent","obregan_parent"]) <= 0) then {
			[] spawn {
				sleep 5;
				// TODO: Update this to check how many hostage tasks were completed
				["victory",true,true,true] remoteExec ["BIS_fnc_endMission", 0, true];
			};
		};
	};
};