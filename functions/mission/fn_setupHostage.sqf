// SXP_fnc_setupHostage
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_unit", nil, [objNull]],
	["_task", "", [""]],
	["_type", "civ", [""]]
];

// If unit is nil, exit the script
if (isNil "_unit") exitWith {};

// Disable the headless client for these units
_unit setVariable ["acex_headless_blacklist", true, true];

[_unit, true] call ace_captives_fnc_setHandcuffed;

// Prevent the AI from speaking
[_unit, "", "ACE_NoVoice", 1, ""] call BIS_fnc_setIdentity;

// Set variables on the unit
_unit setVariable ["sxp_hostage", true, true];
_unit setVariable ["sxp_hostage_task", _task, true];

// Apply the correct unit loadout
switch (toLower _type) do {
	case ("civ"): {
		// Apply loadout
		[_unit, (getMissionConfig "CfgXPT") >> "loadouts" >> "hostage_civ"] call XPT_fnc_loadInventory;
	};
	case ("vip"): {
		// Apply loadout
		[_unit, (getMissionConfig "CfgXPT") >> "loadouts" >> "hostage_vip"] call XPT_fnc_loadInventory;
	};
	case ("military"): {
		// Apply loadout
		[_unit, (getMissionConfig "CfgXPT") >> "loadouts" >> "hostage_military"] call XPT_fnc_loadInventory;
	};
};