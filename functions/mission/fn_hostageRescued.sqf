// SXP_fnc_hostageRescued
// Counts a hostage as "rescued" when they enter the rescue trigger

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_trigger", nil, [objNull]],
	["_list", [], [[]]]
];

/*
	Hostage variables
	sxp_hostage - True/False if the unit is a configured hostage
	sxp_hostage_task - Task that the hostage is associated with
*/

{
	// If the unit is a hostage, update our variables
	if (_x getVariable ["sxp_hostage", false]) then {
		// Determine which task the hostage is associated with, and update the count
		switch (toLower (_x getVariable ["sxp_hostage_task", "none"])) do {
			case "bagango": {sxp_hostage_bagango_rescued = sxp_hostage_bagango_rescued + 1;};
			case "corazol": {sxp_hostage_corazol_rescued = sxp_hostage_corazol_rescued + 1;};
			case "everon": {sxp_hostage_everon_rescued = sxp_hostage_everon_rescued + 1;};
			case "mercalillo": {sxp_hostage_mercalillo_rescued = sxp_hostage_mercalillo_rescued + 1;};
			case "obregan": {sxp_hostage_obregan_rescued = sxp_hostage_obregan_rescued + 1;};
		};
		
		// Delete the hostage once the variable has been updated
		deleteVehicle _x;
	};
} forEach _list;