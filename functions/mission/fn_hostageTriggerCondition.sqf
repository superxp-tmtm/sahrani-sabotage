// SXP_fnc_hostageTriggerCondition
// Returns a condition for the hostage trigger checks
// Must be as lightweight as possible

private _hostageObjects = [];
private _rescuedCount = 0;

switch (_this select 0) do {
	case "bagango": {
		_hostageObjects = (getMissionLayerEntities "Bagango Hostages") select 0;
		_rescuedCount = sxp_hostage_bagango_rescued;
	};
	case "corazol": {
		_hostageObjects = (getMissionLayerEntities "Corazol VIPs") select 0;
		_rescuedCount = sxp_hostage_corazol_rescued;
	};
	case "everon": {
		_hostageObjects = (getMissionLayerEntities "Everon Hostages") select 0;
		_rescuedCount = sxp_hostage_everon_rescued;
	};
	case "mercalillo": {
		_hostageObjects = (getMissionLayerEntities "Mercalillo Hostages") select 0;
		_rescuedCount = sxp_hostage_mercalillo_rescued;
	};
	case "obregan": {
		_hostageObjects = (getMissionLayerEntities "Obregan Hostages") select 0;
		_rescuedCount = sxp_hostage_obregan_rescued;
	};
};

(({alive _x} count _hostageObjects) == 0) || {(({!alive _x} count _hostageObjects) - _rescuedCount) > ((count _hostageObjects) / 2)}