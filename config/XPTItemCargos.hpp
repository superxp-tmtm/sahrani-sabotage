// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.

/* 
	Randomization support
	
	--- Sub-class randomization --- 
	
	Any itemCargos class can define items through a sub-class instead of in the main class.
	In this case, the script will pick one of the sub-classes at random to apply to the object.
	The names of the sub-classes do not matter.
	
	The following example will select one of "box1" or "box2" when the "MyAmmoBox" class is called.
	The names of the subclasses don't matter, and they can be named whatever you want them to be.
	
	class itemCargos
	{
		class MyAmmoBox
		{
			class box1
			{
				items[] = {};
			};
			class box2
			{
				items[] = {};
			};
		};
	};
	
	
	--- Per-item randomization ---
	
	Any item can have the quantity changed from a fixed value, to a randomized value.
	By creating an array of three numbers instead of a single one, the itemCargo script will randomly select a quantity using the provided numbers.
	
	Example:
	
	class MyAmmoBox
	{
		items[] = {"FirstAidKit", {1,3,5}}; // Spawns a minimum of 1, a maximum of 5, and an average of 3
	};

*/
class itemCargos
{
	class example
	{
		// Array containing sub-arrays of items to add
		// Sub-arrays must include an item classname, and a quantity
		// The following would add 5 first aid kits to the inventory of the object
		items[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE basic medical is being used
		itemsBasicMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE advanced medical is being used
		itemsAdvMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
	};
	
	class heli
	{
		items[] = {};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	
	class lav
	{
		items[] = {
			{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",24},
			{"CUP_100Rnd_TE4_LRT4_Yellow_Tracer_762x51_Belt_M",5},
			{"CUP_17Rnd_9x19_glock17",10},
			{"MiniGrenade",10},
			{"SmokeShell",10},
			{"SmokeShellRed",5},
			{"SmokeShellGreen",5},
			{"CUP_1Rnd_HE_M203",8},
			{"CUP_1Rnd_HEDP_M203",8},
			{"1Rnd_Smoke_Grenade_shell",8},
			{"1Rnd_SmokeRed_Grenade_shell",8},
			{"1Rnd_SmokeGreen_Grenade_shell",8},
			{"CUP_launch_M72A6",10}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing",50},
			{"ACE_epinephrine",10},
			{"ACE_adenosine",10},
			{"ACE_morphine",10},
			{"ACE_tourniquet",10},
			{"ACE_bloodIV",3}
		};
		itemsAdvMed[] = {};
	};
	
	class vehicle
	{
		items[] = {
			{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",24},
			{"CUP_100Rnd_TE4_LRT4_Yellow_Tracer_762x51_Belt_M",5},
			{"CUP_17Rnd_9x19_glock17",10},
			{"MiniGrenade",10},
			{"SmokeShell",10},
			{"SmokeShellRed",5},
			{"SmokeShellGreen",5},
			{"CUP_1Rnd_HE_M203",8},
			{"CUP_1Rnd_HEDP_M203",8},
			{"1Rnd_Smoke_Grenade_shell",8},
			{"1Rnd_SmokeRed_Grenade_shell",8},
			{"1Rnd_SmokeGreen_Grenade_shell",8},
			{"CUP_Dragon_EP1_M",2}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing",50},
			{"ACE_epinephrine",10},
			{"ACE_adenosine",10},
			{"ACE_morphine",10},
			{"ACE_tourniquet",10},
			{"ACE_bloodIV",3}
		};
		itemsAdvMed[] = {};
	};
	
	class ammobox
	{
		items[] = {
			{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",42},
			{"CUP_100Rnd_TE4_LRT4_Yellow_Tracer_762x51_Belt_M",12},
			{"CUP_17Rnd_9x19_glock17",3},
			{"CUP_100Rnd_TE1_Yellow_Tracer_556x45_BetaCMag",5}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	
	class ammobox_explosive
	{
		items[] = {
			{"MiniGrenade",20},
			{"SmokeShell",20},
			{"SmokeShellRed",10},
			{"SmokeShellGreen",10},
			{"CUP_1Rnd_HE_M203",15},
			{"CUP_1Rnd_HEDP_M203",15},
			{"1Rnd_Smoke_Grenade_shell",15},
			{"1Rnd_SmokeRed_Grenade_shell",15},
			{"1Rnd_SmokeGreen_Grenade_shell",15},
			{"CUP_Dragon_EP1_M",5},
			{"CUP_launch_M72A6",4}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	
	class ammobox_medical
	{
		items[] = {};
		itemsBasicMed[] = {
			{"ACE_fieldDressing",100},
			{"ACE_epinephrine",25},
			{"ACE_adenosine",25},
			{"ACE_morphine",25},
			{"ACE_tourniquet",10},
			{"ACE_bloodIV",15}
		};
		itemsAdvMed[] = {};
	};
};