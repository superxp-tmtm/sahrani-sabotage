// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class example
{
	title = "Example Task Title";					// Title of task. Displayed as the task name
	description = "Example Task Description";		// Description of task. Additional details displayed when the task is selected on the map screen.
	marker = "";									// Task marker. Leave blank
};

class corazol_parent {
	title = "Corazol";
	description = "";
	marker = "";
};

class corazol_vip {
	title = "Rescue VIPs";
	description = "The ""Guardian"" Royal Guard detachment was ambushed in Corazol while escorting two VIPs. Protect those VIPs and bring them to the FOB when safe to do so.";
	marker = "";
};

class obregan_parent {
	title = "Obregan";
	description = "";
	marker = "";
};

class obregan_military {
	title = "Secure military base";
	description = "SLA extremists have seized control of our military base in Obregan. We need to make sure that we clear and secure the area before they get into our armouries.";
	marker = "";
};

class obregan_hostages {
	title = "Rescue hostages";
	description = "We have reports that the SLA have taken hostages when they attacked the town of Obregan. Secure those hostages and return them to the FOB.";
	marker = "";
};

class bagango_parent {
	title = "Bagango";
	description = "";
	marker = "";
};

class bagango_hostages {
	title = "Rescue hostages";
	description = "We have reports that the SLA have taken hostages when they attacked the town of Bagango. Secure those hostages and return them to the FOB.";
	marker = "";
};

class bagango_tv_station {
	title = "Secure TV station";
	description = "The SLA extremists are broadcasting their demands using over-the-air TV channels, and the only facility on Sahrani that can do that is the Bagango TV station. Be on the lookout for potential hostages.";
	marker = "";
};

class mercalillo_parent {
	title = "Mercalillo";
	description = "";
	marker = "";
};

class mercalillo_hostages {
	title = "Rescue hostages";
	description = "We have reports that the SLA have taken hostages when they attacked the town of Mercalillo. Secure those hostages and return them to the FOB.";
	marker = "";
};

class everon_parent {
	title = "Everon";
	description = "";
	marker = "";
};

class everon_hostages {
	title = "Rescue hostages";
	description = "We have reports that the SLA have taken hostages when they attacked the town of Everon. Secure those hostages and return them to the FOB.";
	marker = "";
};

class everon_military {
	title = "Secure military base";
	description = "SLA extremists have seized control of our military base near Everon. We need to make sure that we clear and secure the area before they get into our armouries.";
	marker = "";
};