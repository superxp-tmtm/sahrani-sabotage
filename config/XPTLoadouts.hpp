// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	// Primary weapons are all G36-based
	// CQC uses a G36C
	// Normal Infantry use G36KA3s
	// SLs and TLs use G36K AG36 (RIS)
	// Marksmen use G36Es with magnified optics
	// Infantry will typically have un-magnified optics
	// CQC uses the AC11704 Red dot
	// Normal uses the M68 CCO (QRP)
	// Magnified infantry use the MRCO
	// Marksmen use the G36 3.5x Optics sight
	
	
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class racs_base: base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"CUP_hgun_Glock17_blk","","CUP_acc_Glock17_Flashlight","",{"CUP_17Rnd_9x19_glock17",17},{},""};
		binocular = "Binocular";
		
		uniformClass = "CUP_U_I_RACS_Desert_1";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemMicroDAGR", "TFAR_anprc148jem", "ItemCompass", "ItemWatch", ""};
		
		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"CUP_17Rnd_9x19_glock17",1,17}};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",2},{"ACE_morphine",1},{"ACE_tourniquet",2}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	// Command Squad base class
	class I_Officer_F: racs_base
	{
		displayName = "Commander";
		
		primaryWeapon[] = {"CUP_arifle_G36KA3","","cup_acc_flashlight","cup_optic_compm2_low",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},""};
		
		uniformClass = "CUP_U_I_RACS_WDL_1";
		headgearClass = "CUP_H_RACS_Beret_Blue";
		facewearClass = "G_Aviator";
		vestClass = "CUP_V_B_Interceptor_Rifleman_Olive";
		backpackClass = "TMTM_TFAR_anprc155_ranger_green";
		
		linkedItems[] = {"ItemMap", "ItemcTab", "TFAR_anprc148jem", "ItemCompass", "ItemWatch", ""};
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30},{"ACE_CableTie",5}};
		backpackItems[] = {};
	};
	
	class cmd_medic: I_Officer_F
	{
		displayName = "Command Medic";
		
		uniformClass = "CUP_U_I_RACS_WDL_2";
		headgearClass = "CUP_H_LWHv2_ERDL_lowland_ess_comms";
		facewearClass = "";
		
		linkedItems[] = {"ItemMap", "ItemcTab", "TFAR_anprc148jem", "ItemCompass", "ItemWatch", ""};
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",15},{"ACE_adenosine",10},{"ACE_morphine",5},{"ACE_bloodIV",5}};
	};
	
	// Logistics
	class I_engineer_F: racs_base
	{
		displayName = "Engineer";
		
		primaryWeapon[] = {"CUP_arifle_G36C_VFG","","cup_acc_flashlight","CUP_optic_AC11704_Black",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},""};
		
		uniformClass = "CUP_U_I_RACS_WDL_2";
		headgearClass = "CUP_H_LWHv2_ERDL_lowland_comms";
		facewearClass = "CUP_G_ESS_CBR";
		vestClass = "CUP_V_B_Interceptor_Rifleman_Olive";
		backpackClass = "B_AssaultPack_rgr";
				
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30}};
		backpackItems[] = {{"ToolKit",1}};
	};
	
	class logi_lead: I_engineer_F
	{
		displayName = "Lead Engineer";
		
		backpackClass = "TMTM_TFAR_anprc155_ranger_green";
		
		linkedItems[] = {"ItemMap", "ItemAndroid", "TFAR_anprc148jem", "ItemCompass", "ItemWatch", ""};
	};
	
	// Royal Guard base class
	class I_G_Soldier_F: racs_base
	{
		displayName = "Royal Guard";
		
		primaryWeapon[] = {"CUP_arifle_G36C_VFG","","cup_acc_flashlight","CUP_optic_AC11704_Black",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},""};
		
		uniformClass = "CUP_U_I_RACS_Desert_3";
		headgearClass = "CUP_H_CZ_Helmet10";
		facewearClass = "CUP_G_ESS_BLK";
		vestClass = "CUP_V_B_PASGT_OD";
		backpackClass = "";
		
		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"MiniGrenade",2,1},{"ACE_M84",2,1},{"SmokeShell",2,1},{"CUP_17Rnd_9x19_glock17",1,17}};
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30},{"ACE_CableTie",5}};
		backpackItems[] = {};
	};
	
	class I_G_Soldier_TL_F: I_G_Soldier_F
	{
		displayName = "Royal Guard Team Lead";
		
		headgearClass = "CUP_H_CZ_Helmet07";
		vestClass = "CUP_V_B_PASGT_no_bags_OD";
		backpackClass = "B_FieldPack_green_F";
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",5,30}};
		backpackItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",3,30},{"SmokeShell",10,1},{"SmokeShellRed",5,1},{"SmokeShellGreen",5,1},{"MiniGrenade",5,1},{"ACE_CableTie",5}};
	};
	
	class I_G_Soldier_M_F: I_G_Soldier_F
	{
		displayName = "Royal Guard Marksman";
		
		primaryWeapon[] = {"CUP_arifle_G36E","","cup_acc_flashlight","CUP_optic_G36Optics_3D",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},"cup_bipod_g36"};
	};
	
	class I_G_Soldier_SL_F: I_G_Soldier_F
	{
		displayName = "Royal Guard Squad Lead";
		
		headgearClass = "CUP_H_CZ_Helmet08";
		facewearClass = "G_Lowprofile";
		vestClass = "CUP_V_B_PASGT_no_bags_OD";
		backpackClass = "TMTM_TFAR_anprc155_ranger_green";
		
		linkedItems[] = {"ItemMap", "ItemAndroid", "TFAR_anprc148jem", "ItemCompass", "ItemWatch", ""};
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",5,30}};
		backpackItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",3,30},{"SmokeShell",10,1},{"SmokeShellRed",5,1},{"SmokeShellGreen",5,1},{"MiniGrenade",5,1},{"ACE_CableTie",5}};
	};
	
	class I_G_Medic_F: I_G_Soldier_F
	{
		displayName = "Royal Guard Medic";
		
		backpackClass = "B_Kitbag_rgr";
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",5,30}};
		backpackItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",3,30}};
		
		basicMedBackpack[] = {{"ACE_fieldDressing",75},{"ACE_epinephrine",25},{"ACE_adenosine",25},{"ACE_morphine",10},{"ACE_bloodIV",10}};
	};
	
	// Urban combat base class
	class urban_rifle: racs_base
	{
		displayName = "Urban Rifleman";
		
		primaryWeapon[] = {"CUP_arifle_G36KA3","","cup_acc_flashlight","cup_optic_compm2_low",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},""};
		
		uniformClass[] = {"CUP_U_I_RACS_mech_1","CUP_U_I_RACS_mech_2"};
		headgearClass = "CUP_H_LWHv2_tigerstripe";
		facewearClass = "CUP_G_ESS_BLK";
		vestClass = "CUP_V_B_Interceptor_Rifleman_Olive";
		backpackClass = "";
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30}};
		backpackItems[] = {};
	};
	
	class urban_at: urban_rifle
	{
		displayName = "Urban Rifleman AT";
		
		secondaryWeapon[] = {"CUP_launch_M72A6", "", "", "", {}, {}, ""};
	};
	
	class urban_tl: urban_rifle
	{
		displayName = "Urban Team Leader";
		
		primaryWeapon[] = {"CUP_arifle_G36K_RIS_AG36","","cup_acc_flashlight","cup_optic_compm2_low",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{"CUP_1Rnd_HE_M203",1},""};
		
		headgearClass = "CUP_H_LWHv2_tigerstripe_comms";
		vestClass = "CUP_V_B_Interceptor_Grenadier_Olive";
		backpackClass = "";
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30},{"CUP_1Rnd_HE_M203",8,1},{"CUP_1Rnd_HEDP_M203",8,1},{"ACE_CableTie",5}};
	};
	
	class urban_sl: urban_tl
	{
		displayName = "Urban Squad Leader";
		
		backpackClass = "TMTM_TFAR_anprc155_ranger_green";
		
		backpackItems[] = {{"1Rnd_SmokeGreen_Grenade_shell",8,1},{"1Rnd_SmokeRed_Grenade_shell",8,1},{"1Rnd_Smoke_Grenade_shell",8,1}};
	};
	
	class urban_medic: urban_rifle
	{
		displayName = "Urban Medic";
		
		headgearClass = "CUP_H_LWHv2_tigerstripe_ess_comms";
		facewearClass = "";
		backpackClass = "B_Kitbag_rgr";
		
		basicMedBackpack[] = {{"ACE_fieldDressing",100},{"ACE_epinephrine",25},{"ACE_adenosine",25},{"ACE_morphine",10},{"ACE_bloodIV",10}};
	};
	
	// Airborne QRF base class
	class airborne_rifle: racs_base
	{
		displayName = "Airborne Rifleman";
		
		primaryWeapon[] = {"CUP_arifle_G36KA3_grip","","cup_acc_flashlight","cup_optic_compm2_low",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},""};
		
		uniformClass[] = {"CUP_U_I_RACS_Urban_1","CUP_U_I_RACS_Urban_2"};
		headgearClass = "CUP_H_LWHv2_Tpat_comms";
		facewearClass = "CUP_G_ESS_BLK_Ember";
		vestClass = "CUP_V_B_Interceptor_Rifleman_Grey";
		backpackClass = "";
		
		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"MiniGrenade",2,1},{"ACE_M84",2,1},{"SmokeShell",2,1},{"CUP_17Rnd_9x19_glock17",1,17}};
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30}};
		backpackItems[] = {};
	};
	
	class airborne_mg: airborne_rifle
	{
		displayName = "Airborne Autorifleman";
		
		primaryWeapon[] = {"CUP_arifle_MG36","","cup_acc_flashlight","cup_optic_eotech553_black",{"CUP_100Rnd_TE1_Yellow_Tracer_556x45_BetaCMag",100},{},""};
		
		vestItems[] = {{"CUP_100Rnd_TE1_Yellow_Tracer_556x45_BetaCMag",6,100}};
	};
	
	class airborne_amg: airborne_rifle
	{
		displayName = "Airborne Assistant Autorifleman";
		
		backpackClass = "B_AssaultPack_blk";
		backpackItems[] = {{"CUP_100Rnd_TE1_Yellow_Tracer_556x45_BetaCMag",8,100}};
	};
	
	class airborne_at: airborne_rifle
	{
		displayName = "Airborne Rifleman AT";
		
		secondaryWeapon[] = {"CUP_launch_M72A6", "", "", "", {}, {}, ""};
	};
	
	class airborne_tl: airborne_rifle
	{
		displayName = "Airborne Team Leader";
		
		primaryWeapon[] = {"CUP_arifle_G36K_RIS_AG36","","cup_acc_flashlight","cup_optic_compm2_low",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{"CUP_1Rnd_HE_M203",1},""};
		
		vestClass = "CUP_V_B_Interceptor_Grenadier_Grey";
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30},{"CUP_1Rnd_HE_M203",8,1},{"CUP_1Rnd_HEDP_M203",8,1},{"ACE_CableTie",5}};
	};
	
	class airborne_sl: airborne_tl
	{
		displayName = "Airborne Squad Leader";
		
		backpackClass = "TMTM_TFAR_anprc155_black";
		
		backpackItems[] = {{"1Rnd_SmokeGreen_Grenade_shell",8,1},{"1Rnd_SmokeRed_Grenade_shell",8,1},{"1Rnd_Smoke_Grenade_shell",8,1}};
	};
	
	class airborne_medic: airborne_rifle
	{
		displayName = "Urban Medic";
		
		headgearClass = "CUP_H_LWHv2_Tpat_ess_comms";
		facewearClass = "";
		backpackClass = "B_TacticalPack_blk";
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",25},{"ACE_adenosine",25},{"ACE_morphine",10},{"ACE_bloodIV",10}};
	};
	
	// Standard infantry base class
	class I_Soldier_F: racs_base
	{
		displayName = "Rifleman";
		
		primaryWeapon[] = {"CUP_arifle_G36KA3","","cup_acc_flashlight","optic_mrco",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},""};
		
		uniformClass[] = {"CUP_U_I_RACS_Desert_1","CUP_U_I_RACS_Desert_2"};
		headgearClass = "CUP_H_LWHv2_desert";
		facewearClass = "CUP_G_ESS_KHK";
		vestClass = "CUP_V_B_Interceptor_Rifleman_Coyote";
		backpackClass = "";
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30}};
		backpackItems[] = {};
	};
	
	class I_Soldier_TL_F: I_Soldier_F
	{
		displayName = "Team Leader";
		
		primaryWeapon[] = {"CUP_arifle_G36K_RIS_AG36","","cup_acc_flashlight","optic_mrco",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{"CUP_1Rnd_HE_M203",1},""};
		
		headgearClass = "CUP_H_LWHv2_desert_comms";
		vestClass = "CUP_V_B_Interceptor_Grenadier_Coyote";
		
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",8,30},{"CUP_1Rnd_HE_M203",8,1},{"CUP_1Rnd_HEDP_M203",8,1},{"ACE_CableTie",5}};
	};
	
	class I_Soldier_SL_F: I_Soldier_TL_F
	{
		displayName = "Squad Leader";
		
		backpackClass = "TMTM_TFAR_anprc155_ranger_green";
		
		backpackItems[] = {{"1Rnd_SmokeGreen_Grenade_shell",8,1},{"1Rnd_SmokeRed_Grenade_shell",8,1},{"1Rnd_Smoke_Grenade_shell",8,1}};
	};
	
	class I_Soldier_AAT_F: I_Soldier_F
	{
		displayName = "Assistant Missile Specialist";
		
		secondaryWeapon[] = {"CUP_launch_M72A6", "", "", "", {}, {}, ""};
		
		backpackClass = "B_Kitbag_cbr";
		
		backpackItems[] = {{"CUP_Dragon_EP1_M",1,1}};
	};
	
	class I_Soldier_AT_F: I_Soldier_AAT_F
	{
		displayName = "Missile Specialist";
		
		secondaryWeapon[] = {"CUP_launch_M47", "", "", "", {"CUP_Dragon_EP1_M",1}, {}, ""};
	};
	
	class I_Soldier_AAR_F: I_Soldier_F
	{
		displayName = "Assistant Machinegunner";
		
		backpackClass = "B_Kitbag_cbr";
		
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_Yellow_Tracer_762x51_Belt_M",6,100}};
	};
	
	class I_Soldier_AR_F: I_Soldier_AAR_F
	{
		displayName = "Machinegunner";
		
		primaryWeapon[] = {"CUP_lmg_Mk48","","cup_acc_flashlight","cup_optic_eotech553_black",{"CUP_100Rnd_TE4_LRT4_Yellow_Tracer_762x51_Belt_M",100},{},""};
		
		backpackClass = "B_AssaultPack_cbr";
		
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_Yellow_Tracer_762x51_Belt_M",3,100}};
	};
	
	class I_Soldier_M_F: I_Soldier_F
	{
		displayName = "Marksman";
		
		primaryWeapon[] = {"CUP_arifle_G36E","","cup_acc_flashlight","CUP_optic_G36Optics_3D",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},"cup_bipod_g36"};
	};
	
	class I_medic_F: I_Soldier_F
	{
		displayName = "Medic";
		
		headgearClass = "CUP_H_LWHv2_desert_ess_comms";
		facewearClass = "";
		backpackClass = "B_Kitbag_cbr";
		
		basicMedBackpack[] = {{"ACE_fieldDressing",100},{"ACE_epinephrine",25},{"ACE_adenosine",25},{"ACE_morphine",10},{"ACE_bloodIV",10}};
	};
	
	// Vehicle crew base class
	class I_crew_F: racs_base
	{
		displayName = "Crewman";
		
		primaryWeapon[] = {"CUP_arifle_G36C_VFG","","cup_acc_flashlight","",{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",30},{},""};
		
		uniformClass = "CUP_U_I_RACS_PilotOverall";
		headgearClass = "CUP_H_CVCH_des";
		facewearClass = "";
		vestClass = "CUP_V_B_Interceptor_Rifleman_Coyote";
		backpackClass = "B_AssaultPack_cbr";
		
		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"SmokeShell",2,1},{"CUP_17Rnd_9x19_glock17",1,17}};
		vestItems[] = {{"CUP_30Rnd_TE1_Yellow_Tracer_556x45_G36",5,30}};
		backpackItems[] = {{"Toolkit",1}};
	};
	
	class I_Helipilot_F: I_crew_F
	{
		displayName = "Helicopter Pilot";
		
		headgearClass = "CUP_H_SPH4_khaki_visor";
		facewearClass = "";
		vestClass = "CUP_V_B_Interceptor_Base_Coyote";
		backpackClass = "TMTM_TFAR_anprc155_ranger_green";
	};
	
	class hostage_civ: base
	{
		displayName = "Civilian Hostage";
		
		uniformClass[] = {
			"U_C_ArtTShirt_01_v6_F",
			"U_C_ArtTShirt_01_v1_F",
			"U_C_Man_casual_2_F",
			"U_C_ArtTShirt_01_v2_F",
			"U_C_ArtTShirt_01_v4_F",
			"U_C_Man_casual_3_F",
			"U_C_Man_casual_1_F",
			"U_C_ArtTShirt_01_v5_F",
			"U_C_ArtTShirt_01_v3_F",
			"U_C_Man_casual_6_F",
			"U_C_Man_casual_4_F",
			"U_C_Man_casual_5_F",
			"U_C_Poloshirt_blue",
			"U_C_Poloshirt_tricolour",
		};
		
		linkedItems[] = {"", "", "", "", "ItemWatch", ""};
	};
	
	class hostage_vip: base
	{
		displayName = "VIP Hostage";
		
		uniformClass[] = {"U_C_FormalSuit_01_black_F","U_C_FormalSuit_01_blue_F","U_C_FormalSuit_01_gray_F","U_C_FormalSuit_01_khaki_F"};
		
		linkedItems[] = {"", "", "", "", "ItemWatch", ""};
	};
	
	class hostage_military: base
	{
		displayName = "Military Hostage";
		
		uniformClass[] = {
			"CUP_U_I_RACS_Desert_1",
			"CUP_U_I_RACS_Desert_2",
			"CUP_U_I_RACS_mech_1",
			"CUP_U_I_RACS_mech_2",
			"CUP_U_I_RACS_Urban_1",
			"CUP_U_I_RACS_Urban_2",
			"CUP_U_I_RACS_WDL_1",
			"CUP_U_I_RACS_WDL_2"
		};
		
		linkedItems[] = {"", "", "", "", "ItemWatch", ""};
	};
};