// Script for creating player tasks
// Only to be run on the server. BIS_fnc_taskCreate is global.
if (!isServer) exitWith {};

// Example task syntax below
/*
[
	true, // Owners of the task. See wiki page for details
	["task name", "parent task name"], // Name of the task, along with parent name. Parent name is used for nested tasks
	["description", "title", "marker"], // Information about the task. Honestly don't know what the marker does. Leave it blank. Can also use a CfgTaskDescriptions class (class name in string form) instead of this array.
	[0,0,0], // Task destination, can also refer to object location. Good method to use is getMarkerPos. Use objNull for task without location.
	"CREATED", // Task state. Current state of task at the time it's created (usually either "CREATED" or "ASSIGNED")
	10, // Task priority. Taken into account when automatically assigning new tasks when previous tasks are completed.
	true, // Show notification. Leave this as true. Set to false to disable task popup
	"attack", // Task type. Types can be found in CfgTaskTypes, or at https://community.bistudio.com/wiki/Arma_3_Tasks_Overhaul#Appendix
	true // Share task. If true, game will report which players have the task selected.	
] call BIS_fnc_taskCreate;

	Make sure to add the name of the zeus unit into the owner field in string format
	It should look like this when written
	
		[true, "zeus_unit"]
		
	This adds the task to all player units, as well as the "zeus_unit" curator.
	This makes sure that zeus units have the same tasks that the players do
*/
// Place tasks here

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	"bagango_parent",
	"bagango_parent",
	objNull,
	"CREATED",
	0,
	false,
	"danger",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["bagango_hostages", "bagango_parent"],
	"bagango_hostages",
	taskloc_bagango_hostages,
	"CREATED",
	10,
	true,
	"meet",
	true
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["bagango_tv_station", "bagango_parent"],
	"bagango_tv_station",
	taskloc_bagango_tv,
	"CREATED",
	10,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	"corazol_parent",
	"corazol_parent",
	objNull,
	"CREATED",
	0,
	false,
	"danger",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["corazol_vip", "corazol_parent"],
	"corazol_vip",
	taskloc_corazol_vip,
	"ASSIGNED",
	100,
	true,
	"meet",
	true
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	"everon_parent",
	"everon_parent",
	objNull,
	"CREATED",
	0,
	false,
	"danger",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["everon_hostages", "everon_parent"],
	"everon_hostages",
	taskloc_everon_hostages,
	"CREATED",
	10,
	true,
	"meet",
	true
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["everon_military", "everon_parent"],
	"everon_military",
	taskloc_everon_military,
	"CREATED",
	10,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	"mercalillo_parent",
	"mercalillo_parent",
	objNull,
	"CREATED",
	0,
	false,
	"danger",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["mercalillo_hostages", "mercalillo_parent"],
	"mercalillo_hostages",
	taskloc_mercalillo_hostages,
	"CREATED",
	10,
	true,
	"meet",
	true
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	"obregan_parent",
	"obregan_parent",
	objNull,
	"CREATED",
	0,
	false,
	"danger",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["obregan_hostages", "obregan_parent"],
	"obregan_hostages",
	taskloc_obregan_hostages,
	"CREATED",
	10,
	true,
	"meet",
	true
] call BIS_fnc_taskCreate;

[
	[true, "zeus_unit_1", "zeus_unit_2", "zeus_unit_3"],
	["obregan_military", "obregan_parent"],
	"obregan_military",
	taskloc_obregan_military,
	"CREATED",
	10,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;