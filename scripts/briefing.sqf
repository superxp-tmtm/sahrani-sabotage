// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

//player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
//player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
//player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];

// TO USE THIS FILE. UNCOMMENT THE MARKED LINE IN "initPlayerLocal.sqf"
player createDiaryRecord ["Diary", ["Mission Notes",
"Hostages must be brought to the courtyard of the ""U-shaped"" building at the base to be counted as rescued. 
<br/><br/>Hostages will periodically be ""cleaned up"" when returned back to the base. This is normal, do not be alarmed if you see them disappear.
<br/><br/>Resupply crates are divided into ammo, explosive, and medical crates. A squad will need one of each of these crates to fully resupply.
<br/><br/>Each of the different types of squads is specialized for certain tasks. The commander must decide which squad is best suited to each task."
]];

player createDiaryRecord ["Diary", ["Assets", "Your assets for this mission are as follows:<br/>
	- 1x UH-1H Slick (Transport)<br/>
	- 1x UH-1H Gunship (Airborne Infantry Transport)<br/>
	- 1x LAV-25 (Mechanized Infantry Transport)<br/>
	- 3x MTVR Transport<br/>
	- 9x Land Rover Transport<br/>
	- 1x MTVR Ammunition (for Logistics use)<br/>
	- 1x MTVR Repair (for Logistics use)<br/>
	- 1x set of repair, rearm, and refuel MTVRs (to service helicopters at base)"
]];

player createDiaryRecord ["Diary", ["Intel",
"The RACS forces are divided up into a number of groups based on their specializations. The groups are detailed below:
<br/><br/><font color='#ff00ff'>Rear Operations Group:</font> Responsible for logistics and overall command of larger RACS battlegroups. Lightly armed, not intended to see direct combat.
<br/><br/><font color='#ff00ff'>Royal Guards Infantry:</font> Highly-trained infantry group specialized in the protection of high-value-targets. Since the six-day conflict, Royal Guard forces have primarily been used for VIP escorts and tactical strikes on insurgent operations.
<br/><br/><font color='#ff00ff'>Airborne Tactical QRF:</font> Combined-arms rapid response force operating primarily out of armed UH-1H Gunship helicopters. These forces are lightweight, highly mobile infantry that can be anywhere they're needed at a moment's notice.
<br/><br/><font color='#ff00ff'>Queen's Mechanized Assault Force:</font> Combined-arms assault force specializing in urban combat. These infantry squads operate attached to an armoured vehicle, and specialize in short-range urban combat with heavy vehicular fire support.
<br/><br/><font color='#ff00ff'>Royal Army Infantry Corps:</font> The ""jack of all trades"" infantry group within the RACS. These all-purpose infantry squads are capable of handling pretty much anything that comes their way."
]];

player createDiaryRecord ["Diary", ["Mission",
"Your main mission today is to stop the SLA uprising in Northern Sahrani.
<br/><br/>Your first priority objective is to rescue the three VIPs that were ambushed in the town of Corazol. A small squad of Royal Guard troops are on-site protecting the VIPs, but they won't last long without assistance.
<br/><br/>Your other objectives are to search the towns for hostages taken by the SLA, and to clear out SLA forces from our military bases to the north. Our armouries at these bases are locked up pretty tight, but there's a lot of still-functional soviet equipment from the six-day conflict left over. You do not want the SLA to get into those armouries."
]];

player createDiaryRecord ["Diary", ["Situation",
"Five months ago, the Sahrani conflict came to an end. With help from the United States Marine Corps, the Royal Army Corps of Sahrani managed to push back the Sahrani Liberation Army's forces to bring an end to the six-day conflict.
<br/><br/>After the conflict, the RACS troops were tasked with keeping the peace while the Democratic Republic of Sahrani was assimilated into the Kingdom of Southern Sahrani, forming a single country. While most of the civilians posed no problem, there were occasional issues that were dealth with swiftly.
<br/><br/>For a couple of months, there was peace in the United Kingdom of Sahrani. The presence of armed forces was slowly beginning to return to normal, until this morning.
<br/><br/>Moments ago, a large number of SLA insurgent cells struck key infrastructure targets, and various towns throughout Northern Sahrani. Many of the RACS forces have been captured, or otherwise rendered combat-ineffective by the surprise attack. A small number of combat-capable troops that escaped the original attack are all that remain."
]];
